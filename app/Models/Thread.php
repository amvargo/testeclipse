<?php

namespace App\Models;

use App\Filters\ThreadFilters;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $guarded = [];

    protected $with = ['creator', 'channel'];

    /*
     * Каскадное удаление данных(Временной, засуну в обсерв, либо на уровне базы)
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('replyCount', function ($builder) {
            $builder->withCount('replies');
        });

        static::deleting(function ($thread) {
            $thread->replies()->delete();
        });
    }

    /*
     * Связь с создателем статьи
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /*
     * Связь с рубриками
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /*
     * Связь с комментариями
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /*
     *  Создать комментарий под статьей
     */
    public function addReply($reply)
    {
        $this->replies()->create($reply);
    }

    # запись уже пролайкана
    public function hasLikedThread()
    {
        return (bool) $this->likes
            ->where('user_id', auth()->user()->id)
            ->count();
    }

    /*
     * Связь с лайками
     */
    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    /*
     * Фильтр(если нужен будет)
     */
    public function scopeFilter($query, ThreadFilters $filters)
    {
        return $filters->apply($query);
    }


    public function hasUpdatesFor($user)
    {
        $key = $user->visitedThreadCacheKey($this);
        return $this->updated_at > cache($key);
    }
}
