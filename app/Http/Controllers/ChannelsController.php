<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use Illuminate\Http\Request;

class ChannelsController extends Controller
{
    public function index()
    {
        $channels = Channel::latest()->get();
        return response()->json($channels);
    }
    public function store(Request $request)
    {
//        $this->validate($request, [
//            'name' => 'required|max:50',
//            'slug' => 'required|max:50',
//        ]);

        $channel = Channel::create([
            'name' => request('name'),
            'slug' => request('slug')
        ]);

        return response()->json($channel);
    }
}
