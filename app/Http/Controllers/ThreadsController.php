<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Thread;
use Illuminate\Http\Request;
use App\Filters\ThreadFilters;

class ThreadsController extends Controller
{
    public function index(Channel $channel, ThreadFilters $filters)
    {
        $threads = $this->getThreads($channel, $filters);
        return response()->json($threads);
    }

    public function store(Request $request)
    {
        /*$this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'channel_id' => 'required|exists:channels,id'
        ]);*/

        $thread = Thread::create([
            'user_id' => auth()->id() ? auth()->id() : 1,
            'channel_id' => request('channel_id'),
            'title' => request('title'),
            'body' => request('body')
        ]);

        return response()->json($thread);
    }

    public function show($channel, Thread $thread)
    {
        if (auth()->check()) {
            auth()->user()->read($thread);
        }
        return response()->json([
            'thread' => $thread,
            'replies' => $thread->replies()->paginate(20),
            'inLike' => auth()->user() ? $thread->hasLikedThread() : false,
        ]);
    }

    public function like($channel, Thread $thread){
        if($thread->hasLikedThread()){
            $this->unlike($channel, $thread);
        }else {
            $thread->likes()->create(['user_id' => auth()->user()->id]);
        }
        return response()->json(['likes' => $thread->likes()->count()]);
    }

    protected function unlike($channel, Thread $thread)
    {
        $thread->likes->where('user_id', auth()->id())->first()->delete();
    }

    public function destroy($channel, Thread $thread)
    {
        $this->authorize('update', $thread);

        $thread->delete();

        if (request()->wantsJson()) {
            return response([], 204);
        }

        return redirect('/threads');
    }

    protected function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);
        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }

        return $threads->get();
    }
}
