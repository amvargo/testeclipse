<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    /*
     *
     */
    public function store($channelId, Thread $thread)
    {
        //$this->validate(request(), ['body' => 'required']);
       $reply = $thread->addReply([
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

       return response()->json($reply);

    }

    public function destroy(Reply $reply)
    {
        $this->authorize('update', $reply);
        $reply->delete();
        return response()->json(['status' => 'success']);
    }
}
