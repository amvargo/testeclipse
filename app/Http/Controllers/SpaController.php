<?php

namespace App\Http\Controllers;

class SpaController extends Controller
{
    /**
     * Инициализация SPA view
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return view('spa');
    }
}
