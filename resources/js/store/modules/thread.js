import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  channels: [],
  threads: [],
  thread: null,
  likes: 0
}

// getters
export const getters = {
  threads: state => state.threads,
  thread: state => state.thread,
  channels: state => state.channels,
  likes: state => state.likes
}

// mutations
export const mutations = {

  [types.GET_THREADS](state, {threads}) {
    state.threads = threads
  },
  [types.GET_THREAD](state, {thread}) {
    state.thread = thread
  },
  [types.GET_CHANNELS](state, {channels}) {
    state.channels = channels
  },
  [types.SET_LIKE_COUNT](state, {likes}) {
    state.likes = likes
  }

}

// actions
export const actions = {
  async like({commit}, request) {
    try {
      return new Promise((resolve, reject) => {
        axios.get(`/api/threads/${request.channel}/${request.thread}/like`).then(response => {
          commit(types.SET_LIKE_COUNT, {likes: response.data.likes});
          resolve(response.data.likes)
        });
      })
    } catch (e) {
    }
  },

  async getThreads({commit}, thread) {
    try {
      commit(types.GET_THREADS, {thread: []})
      const {data} = await axios.get(`/api/threads/${thread}`)
      commit(types.GET_THREADS, {threads: data})
    } catch (e) {
    }
  },

  async getThread({commit}, request) {
    try {
      if(!request.render || request.render === null) {
        commit(types.GET_THREAD, {thread: null})
      }
      const {data} = await axios.get(`/api/threads/${request.channel}/${request.thread}`)
      commit(types.GET_THREAD, {thread: data})
    } catch (e) {
    }
  },

  async getChannels({commit}) {
    try {
      commit(types.GET_CHANNELS, {channels: []})
      const {data} = await axios.get('/api/channels')
      commit(types.GET_CHANNELS, {channels: data})
    } catch (e) {
    }
  },
}
